var Grupo=function(_clave,_nombre,_materias
 ){
  return{
    "clave":_clave,
    "nombre":_nombre,
    "materias":_materias
  };
};

var Materias=function(_clave,_nombre){
  return {
    
    "clave":_clave,
    "nombre":_nombre
  };

};

var grupo1=Grupo('G12','C');
var materia1=Materias('M03','Fisica');
var materia2=Materias('M05','Historia');
var materia3=Materias('M07','mate');
agregarMateriaEnGrupo(grupo1,materia1);

/*Agregar materia al grupo */
function agregarMateriaEnGrupo(g,m){   
  if (!g.materias){   
    g.materias = [];                   
  }     
  if (!exiteMateriaEnGrupo (g, m.clave)){     
    g.materias.push(m); 
    //agregar materia g.materias*/     
    return "La materia "+  "  " +m.nombre+ " " + "se agrego correctamente";   
  }   
  return "La materia "+  "  " +m.nombre+ " " + "ya existe"; 
}  

function exiteMateriaEnGrupo(g,cm){   
  if(!g.materias && g.materias.length===0)
  {     
    return false;   
  }   
    for (var i=0; i<g.materias.length; i++)
    {     
      if (g.materias [i].clave===cm)
      {       
        return true;     
      }   
    }   
  return false; 
}

/*Eliminar materia al grupo*/


function eliminarMateriaDelGrupo(g,m){ 
  if (!exiteMateriaEnGrupo (g, m.clave))
  {    
    return "La materia "+  "  " +m.nombre+ " " + "no existe";
  }  
  else {
   var msj;
    for (var i=0; i<g.materias.length; i++){ 
        if (g.materias [i].clave===m.clave)
        {
         
            g.materias.splice(i, 1);
            msj="Se elimino la materia "+m.nombre;
        }
      else 
        {
        msj="La materia que intentas eliminar no existe"
        }
    } 
    return msj;  
  }
}
eliminarMateriaDelGrupo(grupo1,materia1);
eliminarMateriaDelGrupo(grupo1,materia2);
/* Actualizar datos Meteria */

function modificarMateriaDelGrupo(g,m,clv, nombre){   

  if (!exiteMateriaEnGrupo (g, m.clave))
  {    
    return "La materia "+  "  " +m.nombre+ " " + "no existe";
   
  }  
  else 
  {
    for (var i=0; i<g.materias.length; i++){ 
      g.materias [i].clave =clv;
      g.materias [i].nombre =nombre;
      
      return  "Se Actualizo la materia "+m.nombre;
     
    }    
  }  
   
}

modificarMateriaDelGrupo(grupo1,materia1, 'M01','Fisica');

